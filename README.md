- This repo contains a multi-node multi-core Parallel and a single-node single-core Sequential Java program to calculate the page ranks of web pages in a web network by performing Monte Carlo simulations.
- This program makes use of Parallel Java 2 Library (http://www.cs.rit.edu/~ark/pj2.shtml)
- Achieved near ideal Strong Scaling and Weak Scaling efficiency for the Parallel Program

More details on this project can be found here:
https://sites.google.com/a/g.rit.edu/csci654-team_orion/

Contact: barinder.hanspal@gmail.com for any questions