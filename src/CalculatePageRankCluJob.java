//******************************************************************************
//
// File:    CalculatePageRankCluJob.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

import edu.rit.pj2.Job;
import edu.rit.pj2.Node;
import edu.rit.pj2.Rule;
import edu.rit.pj2.TaskSpec;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class CalculatePageRankCluJob is a multi-node, multi-core parallel program 
 * that is used to calculate the Page Rank of web pages(vertices) in the graph
 * (world wide web)
 * 
 * PageRank is an algorithm that was presented by Google to help to rank websites
 * in their search engine. This program uses a variant of the simplified version
 * of the algorithm. The program performs a MonteCarlo simulation by visiting
 * each and every web page in the graph and performing random walks originating
 * from this vertex to other links present in the page. The program follows the
 * MC complete path stopping at dangling nodes algorithm(#4) presented in the 
 * Research paper "Monte Carlo methods in PageRank computation: When one 
 * iteration is sufficient" by K.Avrachenkov, N. Litvak, D. Nemirovsky,
 * N. Osipova (http://www-sop.inria.fr/members/Konstantin.Avratchenkov/pubs/mc.pdf) 
 * 
 * This MonteCarlo Simulation is done using multiple cores on multiple nodes in
 * a cluster to speed up the page rank calculation. This job class defines a
 * rule that is used to initiate the MonteCarlo simulation across the nodes
 * of the cluster and another rule that reduces the result calculated by all
 * the nodes.
 *
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 */
public class CalculatePageRankCluJob extends Job {

	// Constant: Scaling factor for the ParetoSet Generator function
	public static final Integer UNIT_SCALING_FACTOR = 1;
	
	// Constant: Alpha(Scaling factor) for ParetoSet Distribution
	public static final double DEFAULT_ALPHA_FOR_PARETO_SET_GENERATOR = 0.4;

	private GraphGenerator graphGenerator; // Used to generate the graph to display
	private Graph graph; // Graph representing the world wide web

	/**
	 * Main function
	 */
	public void main(String[] args) throws Exception {

		if(args.length != 5)
			usage();

		// Number of nodes on which the MonteCarlo simulation is to be performed
		Integer numberOfNodes = Integer.parseInt (args[0]);

		/* Number of vertices(web pages) to be created in the graph 
		 * (world wide web)
		 */
		Integer numberOfVertices = Integer.parseInt(args[1]);
		
		// The number of MonteCarlo simulations/iterations to be performed
		Long numIterations = Long.parseLong(args[2]);
		
		// The seed used by the random number that generates the graph
		Long rngSeedForGraphGeneration = Long.parseLong(args[3]);
		
		/* The seed used by the random number to perform the walk on the
		 * generated graph
		 */
		Long rngSeedForRandomWalk = Long.parseLong(args[4]);

		// Input Args validation
		if( 
				(numberOfNodes <= 0 || numberOfNodes > 10)
				||
				(numberOfVertices <= 0)
				||
				(numIterations <= 0)
				||
				(rngSeedForRandomWalk <= 0L)
				||
				(rngSeedForGraphGeneration <= 0L)
			)
			usage();
		
				
		Integer scalingFactorForParetoSetDistribution = UNIT_SCALING_FACTOR;
		double alphaForParetoSetGenerator = DEFAULT_ALPHA_FOR_PARETO_SET_GENERATOR;

		graphGenerator = new GraphGenerator(rngSeedForGraphGeneration,
									alphaForParetoSetGenerator);
		graph = graphGenerator.generateGraph(numberOfVertices,
							 scalingFactorForParetoSetDistribution );
		

		try{
			File outputGraphFile = new File("GeneratedInputGraph_" + numberOfVertices 
							+ "_" + rngSeedForGraphGeneration + ".txt"); 
		
			if(outputGraphFile.exists()){
				outputGraphFile.delete();
			} 
		
			outputGraphFile.createNewFile();
			System.out.println("The input graph that was generated with the provided seed "
							+ "has been stored at location : " 
							+ outputGraphFile.getAbsolutePath());
			FileWriter fw = new FileWriter(outputGraphFile.getAbsolutePath());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(graph.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		/* Create rule to perform tasks that divide MonteCarlo iterations across
		 * 'N' number of nodes in the cluster and calculate the page rank
		 * independently. Each node performs calculations in parallel using
		 * multiple/all cores of the node. At the end, the partial result is 
		 * reduced and put into the tuple space.
		 */
		rule (new Rule()
		.task (numberOfNodes, new TaskSpec (CalculatePageRankCluTask.class)
		.args(numberOfVertices + "", numIterations + "",
				rngSeedForGraphGeneration + "", rngSeedForRandomWalk + "",
				scalingFactorForParetoSetDistribution + "",
				alphaForParetoSetGenerator + "")
				.requires (new Node() .cores (Node.ALL_CORES))));

		/*
		 * Create a rule that reduces the results calculated by each node in 
		 * the cluster. This is done by taking the result tuples that were put 
		 * into the tuple space by the nodes and combining the result of all
		 * such tuples
		 */
		rule (new Rule() 
		.atFinish() 
		.task (new TaskSpec (ReduceTask.class)
		.args("" + numberOfNodes, numberOfVertices + "", numIterations + "",
				rngSeedForGraphGeneration + "", rngSeedForRandomWalk + "")
		.runInJobProcess (true)));
	}
	
	/**
	 * This function prints an error message on the command line if the user 
	 * provides an illegal input argument to the program.
	 * 
	 * @throws IllegalArgumentException when invalid input is provided to the 
	 * 									program
	 */
	private void usage() throws IllegalArgumentException
	{
		System.err.println("Usage: java pj2 CalculatePageRankCluJob "
				+ "<numberOfNodes> <numberOfVertices> "
				+ "<numberOfMontecarloIterations> " +
				"<seedForGraphGeneration> <seedForRandomWalk>");
		System.err.println("<numberOfNodes> - Number of nodes ( 0 < N < 11 )");
		System.err.println("<numberOfVertices> - Number of pages ( N > 0 )");
		System.err.println("<numberOfMonteCarloIterations> - Number of MonteCarlo"
				+ " sumulations ( M > 0 )");
		System.err.println("<seedForGraphGeneration> - Random number seed used"
				+ " to create Graph ( Sg > 0 )");
		System.err.println("<seedForRandomWalk> - Random number seed used to"
				+ " walk the graph ( Sw > 0 )");
		throw new IllegalArgumentException();
	}
}