//******************************************************************************
//
// File:    CalculatePageRankCluTask.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

import edu.rit.pj2.LongLoop;
import edu.rit.pj2.Task;
import edu.rit.util.LongRange;

/**
 * The CalculatePageRankCluTask is the worker task that is run across all the 
 * cores of all the nodes in the cluster. This class helps simulate the 
 * MonteCarlo simulation to help compute the PageRank of all the pages in
 * the generated graph(same graph for all the cores on all the nodes).
 * 
 * This class makes use of the parallelFor loop to help divide simulation work
 * across all the cores of the node.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 *
 */
public class CalculatePageRankCluTask extends Task
{
	private GraphGenerator graphGenerator; // Used to generate the graph
	private ReduceTuple result; // Stores the result in the result tuple
	private ReduceTuple localReduce; // Used for local/parallel loop reduction
	private Graph graph; // The graph object that stores the generated graph
	
	/*
	 * The seed used to create the random number generator for performing random 
	 * walks on the graph
	 */
	private Long seedForRng; 
	
	// Variable that is used to divide the MonteCarlo iterations
	private LongRange slice; 

	/**
	 * Getter function for the seed used for the Random number generator
	 * 
	 * @return value of seed used by the random number generator
	 */
	public Long getSeedForRng()
	{
		return seedForRng;
	}

	/**
	 * The setter function that sets the value of seed variable. This variable
	 * is used as a seed for the random number generator
	 * 
	 * @param seedForRng the value to which the seed class variable is to be set
	 */
	public void setSeedForRng(Long seedForRng)
	{
		this.seedForRng = seedForRng;
	}

	/**
	 * Main function
	 */
	public void main(String[] args) throws Exception 
	{
		
		// Stores the number of vertices in the graph
		Integer numberOfVertices = Integer.parseInt(args[0]);
		
		// Stores the number of MonteCarlo iterations
		Long numIterations = Long.parseLong(args[1]);
		
		/* The seed used by the pareto set random number generator to generate
		 * the graph
		 */
		Long rngSeedForGraphGeneration = Long.parseLong(args[2]);
		
		/* The seed used by the random number generator for performing random 
		 * walks on the generated graph in every MonteCarlo iteration
		 */
		Long rngSeedForRandomWalk = Long.parseLong(args[3]);
		
		// Scaling factor used by the pareto set random number generator
		Integer scalingFactorForParetoSetDistribution = Integer.parseInt(args[4]);
		
		// The shaping factor used by the pareto set distribution
		double alphaForParetoSetGenerator = Double.parseDouble(args[5]);
		
		/*
		 * The walk algorithm that this program uses makes 'm' number of walks 
		 * where 'm' is dependent on the number of MonteCarlo iterations. To 
		 * ensure that all the vertices in the generated graph are visited
		 * equally during the MonteCarlo simulation we make sure that the
		 * number of iterations performed is a multiple of the number of 
		 * vertices in the graph. The adjustedNumIterations stores this value.
		 */
		Long adjustedNumIterations = (long) (Math.ceil(numIterations
									/(double) numberOfVertices) 
								* numberOfVertices);
		setSeedForRng(rngSeedForRandomWalk);

		// Generates the exact same graph for a given seed for a number of runs
		graphGenerator = new GraphGenerator(rngSeedForGraphGeneration, 
									alphaForParetoSetGenerator);
		graph = graphGenerator.generateGraph(numberOfVertices,
								scalingFactorForParetoSetDistribution);

		localReduce = new ReduceTuple();
		result = new ReduceTuple();

		// Slice the number of iterations equally across all the cores
		slice = new LongRange (0, adjustedNumIterations)
						.subrange (groupSize(), taskRank());

		long lb = (long) slice.lb();
		long ub = (long) slice.ub();

		parallelFor(lb,ub).exec(new LongLoop()
		{
			ReduceTuple thrResult; // Used to store thread local result
			RandomNumberGenerator rnd; // Used for performing random walks

			public void start()
			{
				thrResult = (ReduceTuple) threadLocal (result);
			}

			public void run(long i)
			{
				/*
				 *  Generates a unique random number sequence generator for each 
				 *  random walk itertion
				 */
				rnd = new RandomNumberGenerator(getSeedForRng() + i);
				
				// Stores 'vertexId' of the start vertex for the random walk
				int seedIndex = (int)i % graph.getTotalNumVertices();
				
				// Stores vertexId of next vertex for the random walk
				int nextIndex = -1;

				for(int walkIndex = 0; walkIndex < graph.getTotalNumVertices(); walkIndex++)
				{

					// Set seed/start vertex
					Vertex seedVertex = graph.getVertex(seedIndex);
					
					if(seedVertex != null)
					{
						if(seedVertex.getConnectionSize() > 0)
						{
							// Get connections from seed vertex
							
							// Randomly select an index from the connections list
							nextIndex = rnd.nextInt(seedVertex.getConnectionSize());
							
							// Get connections of the seed/current vertex
							nextIndex = seedVertex.getConnection(nextIndex);
							
							// Update page rank result for the new vertex
							thrResult.getPageRankResult().addPageRank(nextIndex);
							
							// continue walk; set current from next
							seedIndex = nextIndex;
						}
					}
				}
			}
		});

		// Put the partial page rank result tuple into the tuple space
		putTuple(result);
	}
}
