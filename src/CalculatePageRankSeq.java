//******************************************************************************
//
// File:    CalculatePageRankSeq.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

import edu.rit.pj2.Task;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * The CalculatePageRankSeq class is a single node sequential program 
 * that is used to calculate the Page Rank of web pages(vertices) in the graph
 * (world wide web)
 * 
 * PageRank is an algorithm that was presented by Google to help to rank websites
 * in their search engine. This program uses a variant of the simplified version
 * of the algorithm. The program performs a MonteCarlo simulation by visiting
 * each and every web page in the graph and performing random walks originating
 * from this vertex to other links present in the page. The program follows the
 * MC complete path stopping at dangling nodes algorithm(#4) presented in the 
 * Research paper "Monte Carlo methods in PageRank computation: When one 
 * iteration is sufficient" by K.Avrachenkov, N. Litvak, D. Nemirovsky,
 * N. Osipova (http://www-sop.inria.fr/members/Konstantin.Avratchenkov/pubs/mc.pdf) 
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 * 
 */
public class CalculatePageRankSeq extends Task{

	// Constant: Scaling factor for the ParetoSet Generator function
	public static final Integer UNIT_SCALING_FACTOR = 1;

	// Constant: Alpha(Scaling factor) for ParetoSet Distribution
	public static final double DEFAULT_ALPHA_FOR_PARETO_SET_GENERATOR = 0.4;
	
	private GraphGenerator graphGenerator; // Used to generate the graph
	private PageRankResult pageRankResult; // Stores the resultant page rank

	/*
	 * Used to extract useful information from the page rank calculation(result)
	 */
	private ResultStatistics resultStatistics; 	

	// Used to generate the random number generator for the random walk
	private RandomNumberGenerator rng; 
	
	private Graph graph; // Graph representing the world wide web
	private Long seedForRng; // Seed for the Random number generator

	/**
	 * Default Empty Constructor
	 */
	public CalculatePageRankSeq() {}

	/**
	 * Getter function that returns the seed for the random number generator 
	 * used for performing the random walk
	 * 
	 * @return seed used for the random number generation
	 */
	public Long getSeedForRng()
	{
		return seedForRng;
	}

	/**
	 * Setter function that sets the seed for the random number generator 
	 * used for performing the random walk
	 * 
	 * @param seedForRng the value of seed that is to be used to set the class
	 * 					 variables random number generation variable
	 */
	public void setSeedForRng(Long seedForRng)
	{
		this.seedForRng = seedForRng;
	}

	/**
	 * Main function
	 */
	public void main(String[] args) throws Exception {

		if(args.length != 4)
			usage();

		// Stores the number of vertices in the graph
		Integer numberOfVertices = Integer.parseInt(args[0]);
		
		// Stores the number of MonteCarlo iterations
		Long numIterations = Long.parseLong(args[1]);
		
		/* The seed used by the pareto set random number generator to generate
		 * the graph
		 */
		Long rngSeedForGraphGeneration = Long.parseLong(args[2]);
		
		/* The seed used by the random number generator for performing random 
		 * walks on the generated graph in every MonteCarlo iteration
		 */
		Long rngSeedForRandomWalk = Long.parseLong(args[3]);

		// Scaling factor used by the pareto set random number generator
		Integer scalingFactor = UNIT_SCALING_FACTOR;
		
		// The shaping factor used by the pareto set distribution
		double alphaForParetoSetGenerator = DEFAULT_ALPHA_FOR_PARETO_SET_GENERATOR;

		if(	
				(numberOfVertices  <= 0)
				||
				(numIterations <= 0L)
				||
				(rngSeedForGraphGeneration <= 0L)
				||
				(rngSeedForRandomWalk <= 0L)
				)
			usage();


		/*
		 * The walk algorithm that this program uses makes 'm' number of walks 
		 * where 'm' is dependent on the number of MonteCarlo iterations. To 
		 * ensure that all the vertices in the generated graph are visited
		 * equally during the MonteCarlo simulation we make sure that the
		 * number of iterations performed is a multiple of the number of 
		 * vertices in the graph. The adjustedNumIterations stores this value.
		 */
		Long adjustedNumIterations = (long) (Math.ceil(numIterations
									/(double) numberOfVertices)
								 * numberOfVertices);
		
		setSeedForRng(rngSeedForRandomWalk);

		rng = new RandomNumberGenerator(getSeedForRng());

		// Generates the exact same graph for a given seed for a number of runs
		graphGenerator = new GraphGenerator(rngSeedForGraphGeneration,
									alphaForParetoSetGenerator);
		graph = graphGenerator.generateGraph(numberOfVertices, scalingFactor);
		pageRankResult = new PageRankResult();

		try{
			File outputGraphFile = new File("GeneratedInputGraph_" + numberOfVertices 
							+ "_" + rngSeedForGraphGeneration + ".txt"); 
		
			if(outputGraphFile.exists()){
				outputGraphFile.delete();
			} 
		
			outputGraphFile.createNewFile();
			System.out.println("The input graph that was generated with the provided seed "
							+ "has been stored at location : " 
							+ outputGraphFile.getAbsolutePath());
			FileWriter fw = new FileWriter(outputGraphFile.getAbsolutePath());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(graph.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		RandomNumberGenerator rnd;

		for(long monteCarloIterIndex = 0L;
					monteCarloIterIndex <= adjustedNumIterations;
					monteCarloIterIndex++)
		{	

			// Stores 'vertexId' of the start vertex for the random walk
			int seedIndex = (int)monteCarloIterIndex 
								% graph.getTotalNumVertices();
			
			// Stores vertexId of next vertex for the random walk
			int nextIndex = -1;

			/*
			 *  Generates a unique random number sequence generator for each 
			 *  random walk itertion
			 */
			rnd = new RandomNumberGenerator(getSeedForRng() + monteCarloIterIndex);

			for(int walkIndex = 0; walkIndex < graph.getTotalNumVertices(); walkIndex++)
			{

				Vertex seedVertex = graph.getVertex(seedIndex);

				if(seedVertex != null)
				{
					if(seedVertex.getConnectionSize() > 0)
					{
						// Get connections from seed vertex
						
						// Randomly select an index from the connections list
						nextIndex = rnd.nextInt(seedVertex.getConnectionSize());
						
						// Get connections of the seed/current vertex
						nextIndex = seedVertex.getConnection(nextIndex);
						
						// Update page rank result for the new vertex
						pageRankResult.addPageRank(nextIndex);
						
						// continue walk; set current from next
						seedIndex = nextIndex;
					}
				} 
			}
		}
		
		// Print calculate page rank result
		//System.out.println(pageRankResult);

		int numberOfPages = 50; // top 50 pages
		this.resultStatistics = new ResultStatistics(pageRankResult);
		this.resultStatistics.extractPageRankProbabilities();
		String result = resultStatistics.printTopNProbabilities(this.pageRankResult.pageRankResult.size() > numberOfPages 
								? numberOfPages :
								 this.pageRankResult.pageRankResult.size());
		System.out.println(result);

		try{
			File outputResultFile = new File("PageRankOutput_" + numberOfVertices
							+ "_" + adjustedNumIterations + "_" + rngSeedForGraphGeneration
							 + "_" + rngSeedForRandomWalk + ".txt"); 
		
			if(outputResultFile.exists()){
				outputResultFile.delete();
			} 
		
			outputResultFile.createNewFile();
			System.out.println("The calculated page rank/result "
							+ "has been stored at location : " 
							+ outputResultFile.getAbsolutePath());
			FileWriter fw = new FileWriter(outputResultFile.getAbsolutePath());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This function prints an error message on the command line if the user 
	 * provides an illegal input argument to the program.
	 * 
	 * @throws IllegalArgumentException when invalid input is provided to the 
	 * 									program
	 */
	private void usage() throws IllegalArgumentException
	{
		System.err.println("Usage: java pj2 CalculatePageRankSeq "
				+ "<numberOfVertices> <numberOfMontecarloIterations> " 
				+ "<seedForGraphGeneration> <seedForRandomWalk>");
		System.err.println("<numberOfVertices> - Number of pages ( N > 0 )");
		System.err.println("<numberOfMonteCarloIterations> - Number of "
				+ "MonteCarlo sumulations ( M > 0 )");
		System.err.println("<seedForGraphGeneration> - Random number seed used"
				+ " to create Graph ( Sg > 0 )");
		System.err.println("<seedForRandomWalk> - Random number seed used to "
				+ "walk the graph ( Sw > 0 )");
		throw new IllegalArgumentException();
	}
}