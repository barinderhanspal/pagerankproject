/**
 * @author Pankaj
 */

import edu.rit.pj2.LongLoop;
import edu.rit.pj2.Task;
import edu.rit.util.LongRange;
import edu.rit.util.Random;


public class CalculatePageRankSmp extends Task{

	/**
	 * @param args
	 */
	private GraphGenerator graphGenerator;
	//private PageRankResult pageRankResult;
	private ReduceTuple result;
	private ReduceTuple localReduce;
	private RandomNumberGenerator rng;
	private Graph graph;
	private Long seedForRng;
	private Long seedForPsrng;
	private LongRange slice;

	
	public Long getSeedForRng() {
		return seedForRng;
	}

	public void setSeedForRng(Long seedForRng) {
		this.seedForRng = seedForRng;
	}

	public Long getSeedForPsrng() {
		return seedForPsrng;
	}

	public void setSeedForPsrng(Long seedForPsrng) {
		this.seedForPsrng = seedForPsrng;
	}

	public void main(String[] args) throws Exception {

		//Tested for input: "CalculatePageRankSmp 10 1000 22223"
		//Output:id=0: 162
		//		id=1: 601
		//		id=2: 37
		//		id=9: 100
		Integer numberOfVertices = Integer.parseInt(args[0]);
		Integer scalingFactor = 1;
		Long numIterations = Long.parseLong(args[1]);
		
		Long adjustedNumIterations = (long) (Math.ceil(numIterations/(double) numberOfVertices) * 								numberOfVertices);
		numIterations = adjustedNumIterations;
		setSeedForRng(31111145L);

		rng = new RandomNumberGenerator(getSeedForRng());

		graphGenerator = new GraphGenerator(Long.parseLong(args[2]), 0.4);
		graph = graphGenerator.generateGraph(numberOfVertices, scalingFactor);
		
		
		localReduce = new ReduceTuple();
		result = new ReduceTuple();
		
		
		slice = new LongRange (0,numIterations)
		.subrange (groupSize(), taskRank());

		long lb = (long) slice.lb();
		long ub = (long) slice.ub();
		
		parallelFor(0,numIterations).exec(new LongLoop(){
			ReduceTuple thrResult;
			
	         RandomNumberGenerator rnd;
	        
			public void start()
			{
			
				thrResult= (ReduceTuple) threadLocal (result);
			}

			
			public void run(long i) {
			
				rnd = new RandomNumberGenerator(getSeedForRng()+i);
				
				
				int seedIndex = (int)i%graph.getTotalNumVertices();
				
				int nextIndex = 0;
				
				for(int walkIndex = 0; walkIndex < graph.getTotalNumVertices(); walkIndex++)
				{
					
					Vertex seedVertex = graph.getVertex(seedIndex);
					
					if(seedVertex!=null)
					{
						if(seedVertex.getConnectionSize()>0)
						{
				
							nextIndex = rnd.nextInt(seedVertex.getConnectionSize());
				
							
							nextIndex = seedVertex.getConnection(nextIndex);
				
							thrResult.getPageRankResult().addPageRank(nextIndex);
							
							
							seedIndex = nextIndex;
						}
									}
					
				}

			}
			
			
		});
		
		
	//	putTuple(localReduce);
		System.out.println(result.getPageRankResult());
		
		
		
	}

	
}
