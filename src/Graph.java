//******************************************************************************
//
// File:    Graph.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * The Graph Class is used to represent the web pages on the world wide web
 * as a random(connected/disconnected) graph using the java Map collection. 
 * Since the vertexIds are defined uniquely, we can use a key, value map that
 * stores the vertex id of a given vertex as the key and the value as the 
 * vertex object. This structure is used to represent an adjacency list of web
 * pages and its links.
 * 
 * So this key, value pair map represents a given vertex (web page) and the 
 * out-going links to other web pages(vertexes). There are functions defined 
 * in this class that can be used to add vertex to the graph, add connections
 * between two or more web pages.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 *
 */
public class Graph {

	/* The adjacency list containing vertexes and its connections, stored as a 
	 * Hash map
	 */
	private Map<Integer, Vertex> adjMap;

	/**
	 * Default Constructor
	 */
	public Graph()
	{
		this.adjMap = new HashMap<Integer, Vertex>();
	}

	/**
	 * This function is used to add a new vertex(web page) to the graph(world 
	 * wide web). Before adding any vertex to the graph, checks are made to see
	 * if the graph already contains a vertex with the same vertexId.
	 * 
	 * @param newVertex the new vertex that is to be added to the graph
	 */
	public void addVertex(Vertex newVertex)
	{
		// Add vertex if the vertex does not exists in the map
		if(!this.adjMap.containsKey(newVertex.getVertexId()))
		{
			this.adjMap.put(newVertex.getVertexId(), newVertex);
		}
	}

	/**
	 * This function is used to get the vertex(web page) object from the graph.
	 * The function checks if the graph contains any vertex with its id being
	 * 'vertexId' which is passed as a parameter, if the graph contains the 
	 * given vertex, it returns the vertex object else returns null.
	 * 
	 * @param vertexId the vertexId of the vertex object that is to be looked up
	 * 					in the graph.
	 * @return the Vertex object for the given vertexId
	 */
	public Vertex getVertex(Integer vertexId)
	{
		if(this.adjMap.containsKey(vertexId))
		{
			return this.adjMap.get(vertexId);
		} 
		else
		{
			return null;
		}
	}

	/**
	 * This function is used to return the total number of vertexes (web pages)
	 * in the given graph.
	 * 
	 * @return the size of the adjacency list.
	 */
	public Integer getTotalNumVertices()
	{
		return this.adjMap.size();
	}

	/**
	 * This function is used to add a directed connection(link) between two 
	 * vertexes (web pages) in the graph.
	 * 
	 * @param fromVertex the originating vertex of the directed link
	 * @param toVertex the terminating vertex of the directed link
	 */
	public void addEdge(Vertex fromVertex, Vertex toVertex) 
	{
		if(
				// check if to and from vertex are not null
				(fromVertex != null || toVertex != null)
				&&
				// check if the toValue is a valid vertex and exists in the map
				(this.adjMap.containsKey(toVertex.getVertexId()))
			) 
			{
				fromVertex.addConnection(toVertex.getVertexId());
			}

	}

	/**
	 * Overriding the default toString() method.
	 */
	public String toString()
	{

		StringBuilder sb = new StringBuilder();
		Iterator<Entry<Integer, Vertex>> iter = this.adjMap.entrySet().iterator();

		while (iter.hasNext())
		{
			Entry<Integer, Vertex> entry = iter.next();
			sb.append("id=" + entry.getKey());
			sb.append(':').append(' ');
			sb.append(entry.getValue());

			if (iter.hasNext())
			{
				sb.append('\n');
			}
		}
		return sb.toString();
	}
}