//******************************************************************************
//
// File:    GraphGenerator.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

/**
 * The GraphGenerator class is used to generate the graph(world wide web) that
 * consists of many vertexes(web pages) and directed links between them.
 * The graph generator generated the specified number of vertices and uses
 * the random number generator (that uses a Pareto Set distribution) for making
 * directed connections/links between the vertices. 
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 *
 */
public class GraphGenerator
{

	private Graph graph; // The graph object
	
	// The Pareto set random number generator
	private ParetoSetNumberGenerator psrng; 

	/**
	 * Constructor with arguments to set the seed for the Pareto set random 
	 * number generator and the shaping factor 'alpha' for the pareto set 
	 * distribution.
	 * 
	 * @param seed the seed for the ParetoSetNumberGenerator
	 * @param alpha the shaping factor for the pareto set distribution
	 * @throws Exception when value of alpha is less than or equal to 0.0
	 */
	public GraphGenerator(Long seed, double alpha) throws Exception
	{
		this.graph = new Graph();
		this.psrng = new ParetoSetNumberGenerator(seed, alpha);
	}

	/**
	 * This function is used to generate all the vertices(web pages) in the 
	 * graph(world wide web). The function takes a parameter that specifies the
	 * number of vertices that are to be created in the graph.
	 * 
	 * @param numberOfVertices the number of vertices to be created in the graph
	 */
	private void generateAllVertices(Integer numberOfVertices)
	{
		for(int index = 0; index < numberOfVertices; index ++)
			this.graph.addVertex(new Vertex(index));
	}

	/**
	 * This function is used to make connections between the vertices(pages)
	 * in the graph(world wide web). The connections are made using random
	 * numbers that follow the pareto set distribution. 
	 * The function goes to each and every vertices in the graph and makes a
	 * number of connections(including 0) to other vertices present in the graph
	 * using a heavy tailed distribution(pareto set). 
	 *  
	 * @param numberOfVertices the number of vertices in the graph
	 * @param scalingFactor the scaling factor passed on to the pareto set
	 * 						distribution
	 */
	private void makeConnections(Integer numberOfVertices, Integer scalingFactor)
	{
		for(int index = 0; index < numberOfVertices; index++ )
		{
			int numEdges = psrng.nextIntPareto(numberOfVertices);
			for(int edgesIndex = 0; edgesIndex < numEdges; edgesIndex++)
			{
				this.graph.addEdge(this.graph.getVertex(index), 
							this.graph.getVertex(psrng
								.nextIntPareto(numberOfVertices,
									 	 scalingFactor)));
			}
		}
	}

	/**
	 * This function is a wrapper function that can be used to generate a 
	 * graph that follows the pareto set/heavy tailed distribution. 
	 * 
	 * @param numberOfVertices the number of vertices to be created in the graph
	 * @param scalingFactor the scaling factor to be passed to the pareto set
	 * 						distribution random number generator
	 * @return	a graph whose connections follow a heavy tailed distribution
	 */
	public Graph generateGraph(Integer numberOfVertices, Integer scalingFactor) 
	{
		this.generateAllVertices(numberOfVertices);
		this.makeConnections(numberOfVertices, scalingFactor);
		return this.graph;
	}
}