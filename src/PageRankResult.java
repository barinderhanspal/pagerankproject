//******************************************************************************
//
// File:    PageRankResult.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import edu.rit.pj2.Tuple;

/**
 * The PageRankResult class stores the page ranks for all the given vertices
 * (web pages) in the graph(world wide web). These page ranks are stored in a
 * key, value pair map where the key represents the vertexId(web page id) and
 * the value represents the calculated page rank for the vertex(web page).
 * 
 * This class provides methods to add page rank to the results of a given vertex 
 * and also to combine results of calculated page ranks for the random walks for
 * all the vertices in the graph.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 */
public class PageRankResult extends Tuple implements Serializable 
{
	// Constant: The default page rank count for any web page
	private static final Integer DEFAULT_PAGE_RANK_COUNT = 0;
	
	// The map that stores the calculated page ranks for a vertices in the graph
	Map <Integer, Integer> pageRankResult;

	/**
	 * Default constructor.
	 * Initializes the page rank hash map.
	 */
	public PageRankResult()
	{
		pageRankResult = new HashMap<Integer, Integer> ();
	}

	/**
	 * This function returns the page rank result (in the hashmap) to the calling
	 * function.
	 * 
	 * @return hashmap containing the page rank result
	 */
	public Map<Integer, Integer> getPageRankResult()
	{
		return pageRankResult;
	}
	
	/**
	 * This function is used to calculate the page rank of a given page/vertexId.
	 * The function first checks if the given pageId/vertexId exists in the graph.
	 * If it does not then it puts a key as the vertexId and sets its value to 1
	 * else it gets the current value for the key and increments it by one and
	 * stores it in the same location
	 *  
	 * @param pageId the vertex id for which the page rank value is to be
	 * 					incremented.
	 */
	public void addPageRank(Integer pageId)
	{
		if(!pageRankResult.containsKey(pageId)) 
		{
			pageRankResult.put(pageId, DEFAULT_PAGE_RANK_COUNT + 1);
		}
		else 
		{
			pageRankResult.put(pageId, pageRankResult.get(pageId) + 1);
		}
	}

	/**
	 * This function returns current value of calculated page rank for a given
	 * pageId/vertexId
	 * 
	 * @param pageId the vertex id for which the page rank value is to be returned
	 * @return the calculated page rank for a pageId/vertexId
	 */
	public Integer getPageRank(Integer pageId)
	{
		
		if(!pageRankResult.containsKey(pageId)) 
		{
			return 0;
		}
		else 
		{
			return pageRankResult.get(pageId);
		}
	}
	
	/**
	 * This function overrides the default toString method.
	 */
	public String toString()
	{
		
		StringBuilder sb = new StringBuilder();
		Iterator<Entry<Integer, Integer>> iter = this.pageRankResult.entrySet().iterator();
		
		while (iter.hasNext())
		{
			Entry<Integer, Integer> entry = iter.next();
			sb.append("id=" + entry.getKey());
			sb.append(':').append(' ');
			sb.append(entry.getValue());
		
			if (iter.hasNext())
			{
				sb.append('\n');
			}
		}
		return sb.toString();	
	}
	
	/**
	 * This function is used to combine the result of one PageRankResult object
	 * into another PageRankResult object. This function is basically used
	 * during the reduction process of the parallel program.
	 * 
	 * @param anotherPageRankResult the page rank result that is to be added
	 * 								to the current(this) page rank result
	 * @return the combination of the two PageRankResult objects.
	 */
	public PageRankResult combineResult(PageRankResult anotherPageRankResult)
	{
		
		if(anotherPageRankResult != null)
		{
			if(anotherPageRankResult.getPageRankResult().size() != 0)
			{
				Iterator<Entry<Integer, Integer>> anotherPageIter 
								= anotherPageRankResult
									.getPageRankResult().entrySet().iterator();
				
				while (anotherPageIter.hasNext())
				{
					Entry<Integer, Integer> anotherPageRankEntry
										= anotherPageIter.next();
					
					// If 'this' object already contains the key
					if(this.getPageRankResult()
							.containsKey(anotherPageRankEntry.getKey()))
					{
						this.getPageRankResult()
							.put(anotherPageRankEntry.getKey(),
								(this.getPageRankResult()
										.get(anotherPageRankEntry.getKey())
									+ anotherPageRankEntry.getValue()
								)
							);
					}
					else
						// 'this' object does not contain the key
					{
						this.pageRankResult.put(anotherPageRankEntry.getKey(),
								anotherPageRankEntry.getValue());
					}
				}
			}
		}
		return this;	
	}
}