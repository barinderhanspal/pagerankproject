//******************************************************************************
//
// File:    ParetoSetDistributionPointsGenerator.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import java.util.Random;
import edu.rit.numeric.plot.*;

public class ParetoSetDistributionPointsGenerator {

public static void main(String args[])
{
	ParetoSetNumberGenerator pg = new ParetoSetNumberGenerator(211113L);
	RandomNumberGenerator rng = new RandomNumberGenerator(211113L);
	Random rnd = new Random(211113L);
	int iter = Integer.parseInt(args[0]);
	System.out.println("********************************************************\nPareto Random Number Generator");
	for(long i=0;i<iter;i++)
	{
		System.out.println(pg.nextIntPareto(1000000, 100));
	}
	System.out.println("********************************************************\nPJ2 Random Number Generator");
	for(long i=0;i<iter;i++)
	{
		System.out.println(rng.nextInt(1000000));
	}
	/*System.out.println("********************************************************\nJava Util Random Number Generator");
	for(long i=0;i<1000000;i++)
	{
		System.out.println(pg.nextIntegerPareto());
	}*/
	
	
	}
	
}
