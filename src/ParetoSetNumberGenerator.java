//******************************************************************************
//
// File:    ParetoSetNumberGenerator.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


/**
 * The ParetoSetNumberGenerator class is used to generate a HEAVY TAILED 
 * distributed set of random numbers. These random numbers are to be used to
 * generate the connections between vertices in the graph. The following 
 * article was used to gain more information about the pareto set generation
 * technique:- introcs.cs.princeton.edu/java/98simulation/
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 *
 */
public class ParetoSetNumberGenerator 
{
	// Constant: Default value of alpha for pareto set generator
	private static final double DEFAULT_ALPHA = 0.6; 

	private Long seed; // The seed for the random number generator
	private double alpha; // The shaping factor for the pareto set distribution
	
	/* The random number generator would be used to feed in uniformly distributed
	 * values in the range (0,1] to generate the pareto set distributed random
	 * numbers.
	 */
	private RandomNumberGenerator rng; 

	/**
	 * Default constructor.
	 * Sets the class properties to the default(not necessarily recommended) values.
	 */
	public ParetoSetNumberGenerator() 
	{
		this.seed = Long.MAX_VALUE/2;
		this.alpha = DEFAULT_ALPHA;
		this.rng = new RandomNumberGenerator(this.seed);
	}

	/**
	 * Constructor with arguments to set the seed for the RandomNumberGenerator
	 * class object and generate random numbers using this seed. Alpha is set to 
	 * its default value. 
	 * 
	 * @param seed the seed for the RandomNumberGenerator object
	 */
	public ParetoSetNumberGenerator(Long seed)
	{
		this.seed = seed;
		this.alpha = DEFAULT_ALPHA;
		this.rng = new RandomNumberGenerator(seed);
	}

	/**
	 * Constructor with arguments to set the seed for the RandomNumberGenerator
	 * class object and generate random numbers using this seed. Alpha is set to 
	 * the value provided as the argument of the constructor
	 * 
	 * @param seed  the seed for the RandomNumberGenerator object
	 * @param alpha  the shaping factor for the pareto set distribution
	 * 
	 * @throws Exception when the value of alpha provided is less than or
	 * equal to 0.0
	 */
	public ParetoSetNumberGenerator(Long seed, double alpha) throws Exception
	{
		if(!(alpha > 0.0))
			throw new Exception("Value of alpha = " + alpha + " , is illegal. "
					+ "Alpha cannot be less than or equal to 0.0");
		this.seed = seed; 
		this.alpha = alpha;
		this.rng = new RandomNumberGenerator(seed);
	}
	
	/**
	 * This function returns the seed value that is to be used to generate the
	 * RandomNumberGenerator object.
	 * 
	 * @return the seed used to create the RandomNumberGenerator object
	 */
	public Long getSeed() 
	{
		return seed;
	}

	/**
	 * This function returns the value of alpha(shaping factor) that is used
	 * to generate the pareto set distribution.
	 * 
	 * @return the value of shaping factor used for pareto set generation.
	 */
	public double getAlpha()
	{
		return alpha;
	}
	
	/**
	 * This function returns the next random double value that follows the pareto
	 * set distribution pattern.
	 * 
	 * @return random double value following pareto set distribution
	 */
	public double nextDoublePareto()
	{
		//Reference: introcs.cs.princeton.edu/java/98simulation/     
		 if (alpha <= 0.0)
	            throw new IllegalArgumentException("Value for alpha must be"
	            		+ " positive");
	        
		return  (Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha) - 1.0);
	}
	
	/**
	 * This function returns the next random double value that follows the pareto
	 * set distribution pattern in a given range.
	 * 
	 * @param range the upper limit for the randomly generated number
	 * @return random double value following pareto set distribution limited
	 * 			to a given range
	 */
	public double nextDoublePareto(Long range)
	{
		return  (Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha) - 1.0) 
					% range;
	}
	
	/**
	 * This function returns the next random double value type-casted to type 
	 * long that follows the pareto set distribution pattern.
	 * 
	 * @return random long value following the pareto set distribution
	 */
	public Long nextLongPareto()
	{
		return (long) (Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha)
						- 1.0);
	}
	
	/**
	 * This function returns the next random double value type-casted to type 
	 * long, limited to a given range, that follows the pareto set distribution
	 * pattern.
	 *
	 * @param range the upper limit for the randomly generated number
	 * @return random long value following pareto set distribution limited
	 * 			to a given range
	 */
	public Long nextLongPareto(Long range)
	{
		return (long) (Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha) 
							- 1.0) % range;
	}
	
	/**
	 * This function returns the next random double value type-casted to type 
	 * long, limited to a given range, that follows the pareto set distribution 
	 * pattern. The value is multiplied by a scaling factor before returning so
	 * as to give a choice of using digits after the floating point.
	 * 
	 * @param range the upper limit for the randomly generated number
	 * @param scalingFactor the value with with the generated number is to be
	 * 						multiplies
	 * @return random long value following the pareto set distribution limited 
	 * 			to a given range
	 */
	public Long nextLongPareto(Long range, int scalingFactor)
	{
		return (long) ((Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha) 
							- 1.0) * scalingFactor) % range;
	}
	
	/**
	 * This function returns the next random double value type-casted to type 
	 * Integer that follows the pareto set distribution pattern.
	 * 
	 * @return random Integer value following the pareto set distribution
	 */
	public Integer nextIntegerPareto()
	{
		return (int) (Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha)
							- 1.0);
	}
	
	/**
	 * This function returns the next random double value type-casted to type 
	 * Integer and limited to a given range that follows the pareto set
	 * distribution pattern.
	 *
	 * @param range the upper limit for the randomly generated number
	 * @return random Integer value following pareto set distribution limited
	 * 			to a given range
	 */
	public Integer nextIntPareto(Integer range)
	{
		return (int) (Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha)
							- 1.0) % range;
	}
	
	/**
	 * This function returns the next random double value type-casted to type 
	 * Integer and limited to a given range that follows the pareto set
	 * distribution pattern. The value is multiplied by a scaling factor before 
	 * returning so as to give a choice of using digits after the floating point.
	 * 
	 * @param range the upper limit for the randomly generated number
	 * @param scalingFactor the value with with the generated number is to be
	 * 						multiplies
	 * @return random Integer value following the pareto set distribution
	 * 			limited to a given range
	 */
	public Integer nextIntPareto(Integer range, int scalingFactor)
	{
		return (int) ((Math.pow(1 - this.rng.nextDouble(), -1.0/this.alpha)
							- 1.0) * scalingFactor) % range;
	}
	
}
