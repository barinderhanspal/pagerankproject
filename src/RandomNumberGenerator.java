//******************************************************************************
//
// File:    RandomNumberGenerator.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import edu.rit.util.Random;

/**
 * The RandomNumberGenerator class is used to generate random numbers and is
 * a wrapper class to the edu.rit.util.Random class. The random numbers are to 
 * be used to perform random walks on the created graph. These random walks
 * help us to perform a MonteCarlo simulation on the given graph to find out
 * the page(vertex) rank of all the pages(vertexes) in the graph.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 *
 */
public class RandomNumberGenerator 
{

	// Constant: Default seed for random number generator
	private final Long DEFAULT_SEED = Long.MAX_VALUE/2;
	
	private Random rng; // Random number variable
	
	/**
	 * Default constructor.
	 * Creates the random number generator object with the default seed
	 */
	public RandomNumberGenerator() 
	{
		this.rng = new Random(DEFAULT_SEED);
	}
	
	/**
	 * Constructor with arguments to create the random number generator object
	 * with the seed provided as the parameter
	 * 
	 * @param seed the seed for the random number generator object
	 */
	public RandomNumberGenerator(Long seed)
	{
		this.rng = new Random(seed);
	}
	
	/**
	 * This function returns a random integer value/number in a given range
	 * using the random number generator class variable
	 *  
	 * @param range the value to which the random number is to be limited to
	 * @return random integer number within a given range
	 */
	public int nextInt(int range)
	{
		return this.rng.nextInt(range);
	}
	
	/**
	 * This function returns a random integer value/number using the random 
	 * number generator class variable
	 *  
	 * @return randomly generated integer number
	 */
	public int nextInteger()
	{
		return this.rng.nextInteger();
	}
	
	/**
	 * This function returns a random Long value/number using the random 
	 * number generator class variable
	 *  
	 * @return randomly generated Long number
	 */
	public Long nextLong()
	{
		return this.rng.nextLong();
	}
	
	/**
	 * This function returns a random double value/number using the random 
	 * number generator class variable
	 *  
	 * @return randomly generated double number
	 */
	public double nextDouble()
	{
		return this.rng.nextDouble();
	}
}
