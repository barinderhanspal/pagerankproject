//******************************************************************************
//
// File:    ReduceTask.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import edu.rit.pj2.Task;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * The Reduce Tuple class is used to reduce the result tuples that are
 * put into the tuple space by all the nodes in the cluster that are calculating
 * the page rank. 
 * This class provides functions that help to get the result tuples put into
 * the tuple by the nodes and combine the partial result of the page rank
 * calculation to give the final output
 *  
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 * 
 */
public class ReduceTask extends Task 
{

	PageRankResult finalPageRankResult; // Stores the final page rank result
	
	/*
	 * Used to extract useful information from the page rank calculation(result)
	 */
	ResultStatistics resultStatistics; 

	/**
	 * Default Constructor
	 * Initializes the class variables
	 */
	public ReduceTask() 
	{
		finalPageRankResult = new PageRankResult();
	}

	/**
	 * This function is used to calculate the final page rank result output
	 * but combining the 'partial page rank' containing result tuples put into 
	 * the tuple space by all the nodes in the cluster. 
	 * 
	 * @param numExpectedResultTuple the number of tuples that it is expected
	 * 					to fetch from the tuple space
	 * @return the final page rank result object
	 */
	public PageRankResult reducePageRankTuples(int numExpectedResultTuple)
	{
		ReduceTuple partialReducedResult;
		
		for(int index=0; index < numExpectedResultTuple; index ++) 
		{
			partialReducedResult = ((ReduceTuple) getTuple(index));
			finalPageRankResult.combineResult(partialReducedResult.getPageRankResult());
		}
		return this.finalPageRankResult;
	}
	
	/**
	 * Main Function
	 */
	public void main(String[] args)
	{
		int numberOfPages = 50; // top 50 pages
		this.finalPageRankResult = reducePageRankTuples(Integer.parseInt(args[0]));
		//System.out.println("Final Page Rank");
		//System.out.println(this.finalPageRankResult);
		this.resultStatistics = new ResultStatistics(finalPageRankResult);
		this.resultStatistics.extractPageRankProbabilities();
		
		String result = resultStatistics.printTopNProbabilities(this.finalPageRankResult.pageRankResult.size() > numberOfPages 
								? numberOfPages :
								 this.finalPageRankResult.pageRankResult.size());
	
		System.out.println(result);

		try{
			File outputResultFile = new File("PageRankOutput_" + args[1]
							+ "_" + args[2] + "_" + args[3] + "_" + args[4] + ".txt"); 
		
			if(outputResultFile.exists()){
				outputResultFile.delete();
			} 
		
			outputResultFile.createNewFile();
			System.out.println("The calculated page rank/result "
							+ "has been stored at location : " 
							+ outputResultFile.getAbsolutePath());
			FileWriter fw = new FileWriter(outputResultFile.getAbsolutePath());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}