//******************************************************************************
//
// File:    ReduceTuple.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import java.io.Serializable;
import edu.rit.pj2.Tuple;
import edu.rit.pj2.Vbl;

/**
 * The ReduceTuple class is used to reduce the results calculated by each
 * core of a given node. This is done by combining/adding the result obtained
 * by each core in the node.
 * 
 * This class has functions that help reduce, set or clone tuples in/from the 
 * tuple space
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 * 
 */
public class ReduceTuple extends Tuple implements Vbl, Serializable
{

	private PageRankResult result; // The object storing the page rank result

	/**
	 * Default Constructor
	 * Initializes the class variables
	 */
	public ReduceTuple()
	{
		this.result = new PageRankResult();
	}

	/**
	 * Default Constructor with arguments
	 * The constructor is used to create a new page rank result object and
	 * combine the result in the page rank result object passed as the parameter
	 * to the function.
	 * 
	 * @param result the value to which the page rank result object is to be
	 * 					initialized
	 */
	public ReduceTuple(PageRankResult result)
	{
		this.result = new PageRankResult();
		this.result.combineResult(result);
	}

	/**
	 * This function returns the page rank result of the calling instance
	 * of the object
	 * 
	 * @return the page rank result for the given object
	 */
	public PageRankResult getPageRankResult()
	{
		return this.result;
	}

	/**
	 * This function is used to reduce the page rank result calculated by each
	 * core in a given node. The function combines/adds the result by adding the 
	 * vbl object holding the partial result and combining its contents with the
	 * contents of the calling instance of this class.
	 * 
	 * @param vbl The variable that is to be combined with the calling instance
	 * 				of this class
	 */
	@Override
	public void reduce(Vbl vbl) 
	{
		ReduceTuple reduceTuple  = (ReduceTuple) vbl;		
		this.result = this.result.combineResult(reduceTuple.getPageRankResult());
	}

	/**
	 * This function creates a copy/clone for the given object
	 */
	public Object clone()
	{
		return new ReduceTuple(this.result);
	}

	/**
	 * The set function is used to set the class result class variable with the
	 * value provided in the function parameter
	 * 
	 * @param vbl the value to which the class variable 'result' is to be set
	 */
	@Override
	public void set(Vbl vbl) 
	{
		ReduceTuple reduceTuple  = (ReduceTuple) vbl;
		this.result = reduceTuple.result;	
	}		
}