//******************************************************************************
//
// File:    ResultStatistics.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************


import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

/**
 * The ResultStatistics class is used to implement functions that can extract
 * useful information from the page rank result obtained by running the
 * MonteCarlo simulation. One of the functions that this class is does it
 * calculate the page rank of every page in probabilities in [0,1].
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 */
public class ResultStatistics 
{
	/* Map that stores the page rank result in probabilities in a sorted
	 * descending order
	 */
	Map<Integer, Double> probabilityMap;
	
	// Map that stores the page rank result in sorted descending order
	Map<Integer, Integer> pageRankSortedMap;
	
	/* PageRankResult object that stores the calculate value of the final page 
	 * rank result
	 */
	PageRankResult pageRankResult;
	
	/* Stores the total number of links between all the vertices(web pages) in
	 *  the graph (world wide web)
	 */
	long totalNumLinks;
	
	/**
	 * Constructor with parameter.
	 * This constructor sets the class's PageRankResult variable with the value
	 * provided as the parameter. This constructor also initialized other class
	 * variables.
	 * 
	 * @param pageRankResult the page rank result to be copied into the class
	 *							variable
	 */
	public ResultStatistics(PageRankResult pageRankResult) 
	{
		probabilityMap = new LinkedHashMap<Integer, Double>();
		pageRankSortedMap = new LinkedHashMap<Integer, Integer>();
		this.pageRankResult = pageRankResult;
		this.totalNumLinks = 0L;
	}

	/**
	 * This function is used to extract the page rank result from the class's
	 * pageRankResult variable and put the the results into the pageRankSortedMap
	 * in a descending order.
	 */
	public void extractPageRankToSortedMap() 
	{
		this.pageRankSortedMap = sortMapByValues(this.pageRankResult
								.getPageRankResult());
	}
	
	/**
	 * This function is used to calculate the total number of links for each
	 * and every vertex(web page) in the graph (world wide web) and set the 
	 * class variable with this calculated value.
	 */
	public void extractTotalNumPageLinks()
	{
		Iterator<Entry<Integer, Integer>> iter = this.pageRankSortedMap
									.entrySet().iterator();
		long totalLinks = 0;
		while (iter.hasNext())
		{
			Entry<Integer, Integer> entry = iter.next();
			totalLinks += entry.getValue();
		}
		this.totalNumLinks = totalLinks;
	}
	
	/**
	 * This function populated the page rank probability map 'probabilityMap'
	 * with the values stored in the pageRankSortedMap and dividing this value
	 * with the total number of links in the graph
	 */
	public void extractPageRankProbabilities()
	{
		this.extractPageRankToSortedMap();
		this.extractTotalNumPageLinks();
		Iterator<Entry<Integer, Integer>> iter = this.pageRankSortedMap
									.entrySet().iterator();
		while (iter.hasNext())
		{
			Entry<Integer, Integer> entry = iter.next();
			this.probabilityMap.put(entry.getKey(), 
							(entry.getValue()
								/(double) this.totalNumLinks));
		}
	}
	
	
	/**
	 * This function is used to print the top 'N' number of page ranks and their
	 * page rank probability.
	 * 
	 * @param numberOfPages the number of pages for which the probabilities are
	 * 						to be printed on standard out.
	 * @return String containing probabilites of top n pages.
	 */
	public String printTopNProbabilities(Integer numberOfPages)
	{
		
		int countIndex = 0;
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("#.####");
		System.out.println("\nPrinting top " + numberOfPages
								+ " probabilities(non-zero)");
		Iterator<Entry<Integer, Double>> iter = this.probabilityMap
									.entrySet().iterator();
			
		while (iter.hasNext() && (countIndex < numberOfPages))
		{
			Entry<Integer, Double> entry = iter.next();
			sb.append("id=" + entry.getKey());
			sb.append(':').append(' ');
			sb.append(df.format(entry.getValue()));
			
			if (iter.hasNext())
			{
				sb.append('\n');
			}	
			++countIndex;
		}
		return sb.toString();
	}
	
	/**
	 * This function is a generic map sorted function that is used to sort a
	 * map in a descending order of their values. The map uses a modified
	 * comparator (compare) function to insert values into the map in a 
	 * descending order.
	 * 
	 * @param pageRankResultMap the map to be sorted.
	 * @return a LinkedHashMap that has its elements sorted in a descending order
	 * 			of their value field 
	 */
	public <K, V extends Comparable<V>> Map<K, V> 
							sortMapByValues(final Map<K, V> pageRankResultMap)
	{
		Comparator<K> rankValueComparator =  new Comparator<K>()
		{
		        public int compare(K rankKey1, K rankKey2)
			{
		        	int compareRankValue = pageRankResultMap.get(rankKey2)
		        					.compareTo(pageRankResultMap.get(rankKey1));
		          
				if (compareRankValue == 0) 
					return 1;
		        	else
					return compareRankValue;
		        }
	    };
	    
	    Map<K, V> pageRankSortedOnValues = new TreeMap<K, V>(rankValueComparator);
	    pageRankSortedOnValues.putAll(pageRankResultMap);
	    return new LinkedHashMap<K,V>(pageRankSortedOnValues);
	}
}