//******************************************************************************
//
// File:    Vertex.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

import java.util.ArrayList;

/**
 * The Vertex class is used to represent the 'web pages' in the World Wide Web.
 * Each vertex represents a unique web page on the world wide web. In any such
 * web page there can be links that point to other web pages in the world wide
 * web. This class has properties that help identify a page uniquely by its
 * 'vertexId' field and a list that stores vertexIds that the page points to.
 * 
 * This class also has functions to add links from a page to any other given page
 * (except itself) in the world wide web.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 * @author Pankaj Deshmukh (pbd6595@rit.edu)
 * 
 */
public class Vertex 
{

	// 'vertexId' represents the unique id for a web page.
	private int vertexId;
	
	/* 'connections' list contains list of vertexIds; representing the other
	 *  web pages it points to.
	 */
	private ArrayList<Integer> connections;

	/**
	 * Constructor with parameters.
	 * 
	 * This constructor can be used to create a vertex object with its 'vertexId'
	 * property set to the value passed in the parameter of the constructor 
	 * function
	 * 
	 * @param vertexId
	 */
	public Vertex(int vertexId)
	{
		this.vertexId = vertexId;
		this.connections = new ArrayList<Integer>();
	}

	/**
	 * This functions returns the vertexId of a given instance of the Vertex 
	 * class object
	 * 
	 * @return the vertexId of the Vertex object
	 */
	public int getVertexId()
	{
		return this.vertexId;
	}

	/**
	 * This function is used to add a web page connections/links from the Vertex
	 * object instance that calls this method to any other web page in the graph
	 * 
	 * The add function checks the vertexIds to avoid any self loop and also to
	 * avoid adding any duplicate vertexIds in its connections list.
	 * that calls this method
	 * 
	 * @param vertexId the vertexId to which a connection is to be made
	 */
	public void addConnection(int vertexId)
	{
		
		if(		
			// Avoiding Selfloop
			(this.vertexId != vertexId)
			&&
			// Check if a connection already exists
			(!this.connections.contains(vertexId))
		  )
			this.connections.add(vertexId);
	}

	/**
	 * This functions is used to return the vertexId stored at the given
	 * index in the connection list for the given vertex.
	 * 
	 * @param index the index for which vertexId is to be returned
	 * @return the vertexId at the given index. NULL if does not exist
	 */
	public int getConnection(int index)
	{
		return this.connections.get(index);
	}

	/**
	 * This function returns the connection list for the given(this) vertex
	 * instance. 
	 * 
	 * @return the connection list for the given vertex; Empty list if there are
	 * 			no elements in the list.
	 */
	public ArrayList<Integer> getConnections(){
	
		return this.connections;
	}
	
	/**
	 * This function returns the number of connections/elements in the
	 * 'connection' list of the calling instance of the Vertex object. 
	 * 
	 * @return size of connections list
	 */
	public int getConnectionSize()
	{
		return this.connections.size();
	}

	/**
	 * This function overrides the default toString method.
	 */
	public String toString()
	{
		String returnString = this.vertexId + " --> ";
		if(this.connections == null)
		{
			return "";

		} 
		else if (this.connections.size() == 0)
		{
			return returnString + " - ";
		}
		else 
		{
			return returnString + this.connections;
		}
	}
}